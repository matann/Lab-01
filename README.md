# Microservices Workshop
Lab 01: Setting up the lab infrastructure

---

## Import the repositories:

 - https://github.com/selaworkshops/division-service.git
 
 - https://github.com/selaworkshops/sum-service.git
 
 - https://github.com/selaworkshops/multiplication-service.git
 
 - https://github.com/selaworkshops/subtraction-service.git
 
 - https://github.com/selaworkshops/ui-service.git
 
 - https://github.com/selaworkshops/microservices-calculator-app.git
 


## Create the following repositories in docker hub:

 - <your-username>/division-service
 
 - <your-username>/sum-service
 
 - <your-username>/multiplication-service
 
 - <your-username>/subtraction-service
 
 - <your-username>/ui-service


 
## Create a Personal Access Token (PAT)

 - Open your profile and go to your security details:

 ![Image 1](https://docs.microsoft.com/en-us/vsts/repos/git/_shared/_img/my-profile-tfs.png?view=vsts)

 - Select "Personal Access Token" and click "Add" to create the token:

 ![Image 2](https://docs.microsoft.com/en-us/vsts/repos/git/_shared/_img/add-personal-access-token.png?view=vsts)

 - Create your token (default values): 

 ![Image 3](https://docs.microsoft.com/en-us/vsts/repos/git/_shared/_img/setup-personal-access-token.png?view=vsts)

 - When you're done, make sure to copy the token
 


## Configure Build Agents 	

 - Connext to the build server using ssh:
 
```
$ ssh vsts@<SERVER-IP>
```
```
password: vsts
```

 - Pull the vsts agent image:
 
```
$ docker pull leonjalfon1/vsts-agent
```

 - Create an Agent Pool for the build agents:
 
```
Agent Pool: docker-agents
```

 ![Image 4](images/lab01-4.png)

 - Deploy 5 agents using the command below (and replacing the parameters):
 
```
$ docker run -d \
-e VSTS_ACCOUNT=<myaccount> \
-e VSTS_TOKEN=<my-pat-token> \
-e VSTS_AGENT='$(hostname)-agent' \
-e VSTS_POOL=docker-agents \
-e VSTS_WORK='/var/vsts/$VSTS_AGENT' \
-v /var/vsts:/var/vsts \
-v /var/run/docker.sock:/var/run/docker.sock \
leonjalfon1/vsts-agent
``` 
 
 - Ensure the agents are up and running:
 
 ![Image 5](images/lab01-5.png)

 
 
## Configure Deployment Groups	

 - Browse to the deployment groups page and create a new one:
 
 ![Image 6](images/lab01-6.png)
 
- Click "Register" button to get the register script:

 ![Image 7](images/lab01-7.png)
 
- Connect to the server and run the script:

```
$ ssh vsts@<SERVER-IP>
$ <script>
```

- Add the tag LAB to the server (optional)

- Repeat the same steps to create the another deployment group
 